/*
 * coding: utf-8
 *  SPDX-License-Identifier: GPL-3.0-only
 *  Copyright 2025 dradux.com
 */

use prettytable::{Table, format};
use std::process::Command;
use colored::*;

use crate::conf::*;

pub fn check_packaging(conf: &PrjConfPackaging) {
    // check packaging.

    // @TODO: this could be a spinner (look at indicatif package).
    println!("Inspecting Packaging, this could take a while...");

    let cmd_args: Vec<&str> = conf.command.split_whitespace().collect();
    // pluck first item as base, remaining as params.d
    let (cmd_base, cmd_params) = cmd_args.split_at(1);
    let mut output = Command::new(cmd_base[0]);
    if !conf.root_dir.is_empty() {
        println!("  --> root dir specified....");
        output.current_dir(&conf.root_dir);
    }
    output.args(cmd_params);
    let res = output.output().expect("failed to execute process");
    let res_str = String::from_utf8_lossy(&res.stdout);
    let mut rid = 0;
    let mut table = Table::new();
    table.set_titles(row!["#", "OUTPUT"]);
    table.set_format(format::FormatBuilder::new()
        .column_separator('│')
        .borders('│')
        .separators(&[format::LinePosition::Top],
                 format::LineSeparator::new('─', '┬', '┌', '┐'))
        .separators(&[format::LinePosition::Intern],
                 format::LineSeparator::new('─', '┼', '├', '┤'))
        .separators(&[format::LinePosition::Bottom],
                 format::LineSeparator::new('─', '┴', '└', '┘'))
        .padding(1, 1)
        .build());
    let res_data: Vec<&str> = res_str.lines().collect();
    // NOTE: we cannot format the result in any way as each tools (e.g. pip-review, quasar upgrade, node, etc.) will have their own output style
    for row in res_data {
        rid += 1;
        table.add_row(row![rid, row]);
    }

    // if no rows dont show table.
    if rid > 0 {
        table.printstd();
    }
    if !res.stderr.is_empty() {
        println!("{} {}", "ERROR:".red(), String::from_utf8_lossy(&res.stderr).yellow());
    }
}
