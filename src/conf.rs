/*
 * coding: utf-8
 *  SPDX-License-Identifier: GPL-3.0-only
 *  Copyright 2025 dradux.com
 */

use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

static PRJ_CONF_INIT: &str =
    "# iip config file (generated from `iip --init`)


[image]
    enabled = true
    # limit number of tags returned in image check
    #   max size of 65535
    #   note that tags are returned in 'last updated' order not version order.
    tag_limit = 100
    filename = \"Dockerfile\"

    # filter tags (leave blank for no filter)
    #   - uses standard regex, see [regex crate](https://docs.rs/regex/1.5.4/regex) for more info
    tag_filter = \"\"
    # example tag filter
    #tag_filter = \"^.*-alpine.*$\"

[packaging]
    enabled = false
    # using docker-compose
    #root_dir = \"/home/drad/code/dradux/points\"
    #command = \"docker-compose run --rm api pip-review\"
    # using docker
    root_dir = \"\"
    command = \"docker run --rm=true points_api pip-review\"

";

//#[derive(Serialize, Deserialize, Debug)]
//struct Package {
//    name: String,
//    created: String,
//    modified: String,
//    version: String,
//    authors: Vec<String>,
//    edition: String,
//}

//#[derive(Serialize, Deserialize, Debug)]
//struct InternalConf {
//    package: Package,
//}

#[derive(Serialize, Deserialize, Debug)]
pub struct PrjConf {
    pub image: PrjConfImage,
    pub packaging: PrjConfPackaging,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PrjConfImage {
    pub enabled: bool,
    pub tag_limit: u16,
    pub filename: String,
    pub tag_filter: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PrjConfPackaging {
    pub enabled: bool,
    pub root_dir: String,
    pub command: String,
}

pub fn project_init() {
    // create the project config file.

    let path = Path::new(".iip");
    let display = path.display();
    println!("Creating Project config file...");

    // check to ensure file does not exist.
    if Path::new(path).exists() {
      println!("  file already exists, cannot create!");
    } else {
        // Open a file in write-only mode, returns `io::Result<File>`
        let mut file = match File::create(path) {
            Err(why) => panic!("couldn't create {}: {}", display, why),
            Ok(file) => file,
        };

        // Write the string to `file`, returns `io::Result<()>`
        match file.write_all(PRJ_CONF_INIT.as_bytes()) {
            Err(why) => panic!("couldn't write to {}: {}", display, why),
            Ok(_) => println!("  config file ({}) successfully created in the current directory.", display),
        }
    }

}
