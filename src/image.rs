/*
 * coding: utf-8
 *  SPDX-License-Identifier: GPL-3.0-only
 *  Copyright 2025 dradux.com
 */

use chrono::prelude::*;
use dockerfile_parser::{Dockerfile, StageParent};
use prettytable::{Table, format};
use regex::Regex;
use serde::{Deserialize};
use std::fs::File;

use crate::conf::*;

pub struct ImageListing {
    pub image: String,
    pub tag: String,
}

#[derive(Deserialize, Debug)]
pub struct ImagesCheckResults {
    pub name: String,
    pub last_updated: String,
}

#[derive(Deserialize, Debug)]
pub struct ImagesCheck {
    pub count: u32,
    pub results: Vec<ImagesCheckResults>,
}

pub fn get_images(filename: &str) -> Vec<ImageListing> {
    // Get a set of images from a Dockerfile

    println!("- getting images from file: {}", filename);
    let f = File::open(filename).expect("file must be readable");
    let dockerfile = Dockerfile::from_reader(f)
        .expect("Something went wrong reading Image file");
    let mut result = Vec::new();

    for stage in dockerfile.iter_stages() {
        let sp:StageParent = stage.root;   // --> StageParent (Image)
        match sp {
            StageParent::Image(value) => result.push(ImageListing { image: value.image.clone(), tag: value.tag.clone().unwrap() }),
            _ => println!("Something else"),
        }
    }
    result
}

pub fn check_images(images: Vec<ImageListing>, conf: &PrjConfImage) {
    // check a set of images.

    println!("Inspecting Images, this could take a while...");
    for image in images {
        check_image(image, conf);
    }
}

fn check_image(image: ImageListing, conf: &PrjConfImage) {
    // Check a specific image.

    println!("- checking image: {}:{} (limit={})", image.image, image.tag, conf.tag_limit);
    let image_check_url = format!("https://registry.hub.docker.com/v2/repositories/library/{}/tags/?page=1&page_size={}", image.image, conf.tag_limit);
    let response = reqwest::blocking::get(image_check_url).unwrap();

    let data:ImagesCheck = response.json().unwrap();
    let mut rid = 0;
    let mut total = 0;
    // Create table
    let mut table = Table::new();
    // Set headers
    table.set_titles(row!["#", "TAG", "DATE"]);
    table.set_format(format::FormatBuilder::new()
        .column_separator('│')
        .borders('│')
        .separators(&[format::LinePosition::Top],
                 format::LineSeparator::new('─', '┬', '┌', '┐'))
        .separators(&[format::LinePosition::Intern],
                 format::LineSeparator::new('─', '┼', '├', '┤'))
        .separators(&[format::LinePosition::Bottom],
                 format::LineSeparator::new('─', '┴', '└', '┘'))
        .padding(1, 1)
        .build());

    let mut restr: &str = "^.*$";
    if !conf.tag_filter.is_empty() {
        restr = &conf.tag_filter
    }

    let re = Regex::new(restr).unwrap();

    // NOTE: the natural sort order is by date which is what we want.
    for tag in data.results {
        total += 1;
        let lu = tag.last_updated.parse::<DateTime<Utc>>().unwrap();
        if re.is_match(&tag.name) {
            // highlight row if tag matches tag from image
            if tag.name == image.tag {
                table.add_row(row![bFr->rid, bFr->tag.name, iFy->lu.format("%Y-%m-%d %H:%M").to_string()]);
            } else {
                table.add_row(row![rid, tag.name, lu.format("%Y-%m-%d %H:%M").to_string()]);
            }
            rid += 1;
        }
    }
    // print table.
    table.printstd();
    if !conf.tag_filter.is_empty() {
        println!("Filtered tags: {}", (total - rid));
    }
    println!("Total tags:    {}", data.count);
}
