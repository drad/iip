/*
 * coding: utf-8
 *  SPDX-License-Identifier: GPL-3.0-only
 *  Copyright 2025 dradux.com
 */

#[macro_use] extern crate prettytable;

use std::error::Error;
use std::env;
use std::fs;
use std::io;
use std::io::Write;
use std::path::Path;
use std::process;
use text_io::read;

mod conf;
mod image;
mod packaging;
use crate::conf::*;
use crate::image::*;
use crate::packaging::*;

static ABOUT_MODIFIED: &str = "2025-02-04";

fn get_user_input(prompt: &str) -> String {
    print!("{}", prompt);
    io::stdout().flush().unwrap();
    read!("{}\n")
}

fn summary(prj_conf: &PrjConf) {
    println!("
------------ SUMMARY ------------
- Image
  - inspect:    {}
  - filename:   {}
  - tag limit:  {}
  - tag filter: {}
- Packaging
  - inspect:    {}
  - root dir:   {}
  - command:    {}
", prj_conf.image.enabled,
    prj_conf.image.filename,
    prj_conf.image.tag_limit,
    prj_conf.image.tag_filter,
    prj_conf.packaging.enabled,
    prj_conf.packaging.root_dir,
    prj_conf.packaging.command);

    let cont = get_user_input("Do you want to continue? [y/n]: ");
    if cont != "y" {
        println!("Process canceled");
        process::exit(3);
    }
}

fn main() -> Result<(), Box<dyn Error + 'static>> {
    for argument in env::args() {
        if argument == "--version" {
            println!("{} - version: {} ({})", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"), ABOUT_MODIFIED);
            process::exit(0);
        }
        if argument == "--help" {
            println!("{} [OPTIONS]
  {}

OPTIONS:
  --init: creates the skeleton project config file in the current (execution) directory
  --version: prints program version
  --help: prints this help message

", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_DESCRIPTION"));
            process::exit(0);
        }
        if argument == "--init" {
            project_init();
            process::exit(0);
        }
    }

    let path = Path::new(".iip");
    if !Path::new(path).exists() {
        println!("WARNING: project conf file not found, cannot continue.\n\nTip: use '--init' to create a new project conf file.\n");
        process::exit(1);
    }

    println!("Inspect Image and Packaging (IIP)");

    let prj_conf_file = fs::read_to_string(".iip")
        .expect("Something went wrong reading project config file");

    let prj_conf: PrjConf = toml::from_str(&prj_conf_file).unwrap();

    summary(&prj_conf);

    if prj_conf.image.enabled {
        check_images(get_images(&prj_conf.image.filename), &prj_conf.image);
    }

    if prj_conf.packaging.enabled {
        check_packaging(&prj_conf.packaging);
    }

    Ok(())
}
