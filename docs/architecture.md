# Architecture

### Requirements

The requirements are minimal and dependent upon what you inspect:

- if inspecting images:
    + external (e.g. pip-review, etc.)
- if inspecting packages:
    + docker

#### Docker

Docker is required to inspect images as we leverage docker to run an instance of the image being inspected to check for outdated packages.

### System

- single rust binary with the following libraries included:
    + dockerfile-parser: parses dockerfile


### Flow

At the highest level, the flow is as follows:

- inspect image
    - for each stage
        - get image and tag
        - list tags available for image on dockerhub
- inspect package
