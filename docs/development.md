# Development

This project uses rustup, cargo and [cargo-audit](https://lib.rs/crates/cargo-audit) for dependency management.

### Update Packages

- `cargo update --dry-run`   # note: this will update packages to latest possible given Cargo.toml [version requirements](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html)
- update app version in `Cargo.toml`


### Lint

- `cargo clippy`
    - to fix: `cargo clippy --fix`


### Audit

-  `cargo audit fix --dry-run`


### Build

- `cargo run -- --version`
    + note: this will set the `modified` envvar for the app
- release: `cargo build --release && strip target/release/iip && strip target/release/iip`
    + __note:__ this will automatically 'deploy' the built release *if* you have it symlinked to the `target/release/iip` binary


### Links

- [Clippy](https://github.com/rust-lang/rust-clippy)
- [more on clippy](https://nnethercote.github.io/perf-book/linting.html)
- [audit](https://lib.rs/crates/cargo-audit)
