# Readme

Inspect Image and Packaging (IIP) inspects packages (python, ruby, node, etc) and images (docker) of a project.


### Latest Changes

- package updates: aho-corasick, autocfg, backtrace, base64, bitflags, bumpalo, bytes, cc, chrono, encoding_rs, fastrand, getrandom, h2, hashbrown, hermit-abi, http, http-body, hyper, iana-time-zone, iip, indexmap, is-terminal, itoa, js-sys, libc, libredox, linux-raw-sys, log, memchr, miniz_oxide, mio, num-traits, openssl, pest, pkg-config, proc-macro2, quote, redox_users, regex, reqwest, rustix, rustversion, ryu, security-framework, serde, socket2, syn, tempfile, thiserror, tokio, toml, unicode-bidi, wasm-bindgen, web-sys, windows-targets, winnow, winreg


### Image Inspection

iip uses docker to parse your `Dockerfile` to show the image version and then queries dockerhub to get tags for this image - allowing you to compare what you have against what is available.


### Building

- development: `cargo run` (to check version use `cargo run -- --version`)
- release: `cargo build --release && strip target/release/iip && strip target/release/iip`

__NOTE:__ see `docs/development.md` for more info.


### Setup

There are various ways to get iip on your `$PATH`:

- copy the `target/release/iip` file to `/usr/bin`
- add `target/release/iip` to your `$PATH`
- or create a symlink: `ln -sf -T "/opt/code/dradux/iip/target/release/iip" "$HOME/bin/iip"`
    + the above symlink is a 'user' symlink and assumes `~/bin` is already in yoru `$PATH`
    + adjust the source (`/opt/code/dradux/iip/target/release/iip`) of the above symlink to reference where your IIP binary is located


### Links

- [dockerfile_parser](https://crates.io/crates/dockerfile-parser)
- [reqwest](https://docs.rs/reqwest/0.11.3/reqwest/)
- [colored](https://crates.io/crates/colored)
- [prettytable-rs](https://crates.io/crates/prettytable-rs)
- [regex](https://docs.rs/regex/1.5.4/regex)
